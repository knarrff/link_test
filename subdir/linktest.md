not working:

- [link to current directory `./`](./)
- [relative link to current directory via child without dot `subsubdir/../`](subsubdir/../)
- [relative link to current directory via child including dot `./subsubdir/../`](./subsubdir/../)

working:

- [relative link to current directory via parent `../subdir/`](../subdir/)
- [relative link to file within current directory without dot `linktest.md`](linktest.md)
- [relative link to file within current directory including dot `./linktest.md`](./linktest.md)
- [relative link to subdirectory without dot `subsubdir/`](subsubdir/)
- [relative link to subdirectory including dot `./subsubdir/`](./subsubdir/)
