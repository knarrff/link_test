not working:

- [link to current directory](./)
- [relative link to current directory via child without dot](subsubsubdir/../)
- [relative link to current directory via child including dot](./subsubsubdir/../)

working:

- [relative link to current directory via parent](../subsubdir/)
- [relative link to file within current directory without dot](linktest.md)
- [relative link to file within current directory including dot](./linktest.md)
